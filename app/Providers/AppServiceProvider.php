<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        try {
            $redis = new \Redis();
            $redis->connect(env('REDIS_HOST','127.0.0.1'), env('REDIS_PORT',6379));
            $redis->auth(env('REDIS_PASSWORD', null));
        } catch (\Exception $exception) {
            dd($exception->getMessage());
        }
    }
}
