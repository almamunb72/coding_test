<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RedisController extends Controller
{
    protected $redis;
    protected $ttl;
    protected $exception;

    public function __construct()
    {
        try {
            $this->redis = new \Redis();
            $this->redis->connect(env('REDIS_HOST'), env('REDIS_PORT'));
            $this->redis->auth(env('REDIS_PASSWORD'));
            $this->ttl = intval(env('REDIS_TTL', 5*60));
        } catch (\Exception $exception) {
            return response(['error' => $exception->getMessage()], 500);
        }

    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            if ($request->get('keys')) {
                $stored_items = $this->show($request->get('keys'));
                return response($stored_items, 200);
            }
            $stored_keys = $this->redis->keys('*');
            if (!$stored_keys)
                return response([], 200);
            $stored_items = $this->getAndUpdateTtl($stored_keys);
            return response($stored_items, 200);
        } catch (\Exception $exception) {
            return response($exception->getMessage(), 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            if ($request->all()) {
                $this->redis->msetnx($request->all());
                foreach ($request->all() as $key => $value) {
                    $this->updateTtl($key);
                }
                return response(['success' => 'created'], 200);
            }
            return response(['error' => 'No Data!'], 400);
        } catch (\Exception $exception) {
            return response($exception->getMessage(), 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  string $keys
     * @return array || null
     */
    public function show($keys)
    {
        $user_keys = explode(',', $keys);
        $found_items = [];
        foreach ($user_keys as $key) {
            $value = $this->redis->get($key);
            if ($value) {
                $found_items[$key] = $value;
                $this->updateTtl($key);
            }
        }
        return $found_items;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try {

            if (!$request->all())
                return response(['success' => 'Nothing to update'], 400);
            $updated = [];
            $not_found = [];
            foreach ($request->all() as $key => $value) {
                if ($this->redis->exists($key)) {
                    $this->redis->set($key, $value, $this->ttl);
                    $updated[] = $key;
                } else
                    $not_found[] = $key;
            }
            if ($updated && $not_found) {
                $status = 200;
                $msg = implode(",", $updated) . " keys are updated! and " . implode(",", $not_found) . " keys are not found!";
            } elseif ($updated && !$not_found) {
                $status = 200;
                $msg = implode(",", $updated) . " keys are updated!";
            } else
            {
                $status = 400;
                $msg = implode(",", $not_found) . " keys are not found!";
            }
            return response(['success' => $msg], $status);
        } catch (\Exception $exception) {
            return response(['error' => $exception->getMessage()], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function getAndUpdateTtl(array $array)
    {
        $found_items = array_combine($array, $this->redis->mget($array));
        foreach ($array as $item)
            $this->updateTtl($item);
        return $found_items;

    }

    private function updateTtl(string $key)
    {
        $this->redis->expireAt($key, time() + $this->ttl);
    }
}
