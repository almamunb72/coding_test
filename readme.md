<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## REST key-val store
Create an API with the following endpoints to create a key-value store. The purpose of the API is to store any arbitrary length value in a persistent store with respect to a key and later fetch these values by keys. These values will have a TTL (for example 5 minutes) and after the TTL is over, the values will be removed from the store.

### GET /values
Get all the values of the store.

response: {key1: value1, key2: value2, key3: value3...}

### GET /values?keys=key1,key2
Get one or more specific values from the store and also reset the TTL of those keys.

response: {key1: value1, key2: value2}

### POST /values
Save a value in the store.

request: {key1: value1, key2: value2..}
response: whatever’s appropriate

### PATCH /values
Update a value in the store and also reset the TTL.

request: {key1: value1, key2: value2..}
response: whatever’s appropriate

### Constraints
#### Must be done
Use appropriate status codes with all the responses.
Values can be of arbitrary length
Remove all values stored over more than 5 minutes. Set a TTL.
Has to be FAST
#### Good to have
Reset TTL on every GET Request.
Must be fault-tolerant, persistent
It’s a plus if you can run the service with single command
writing test case is also encourged if possible
Published on

## Server Requirements

- php 7.2+
- redis 4.0.9
- laravel 5.7.*

#### base url = your_server_url:port/api/redis
#### command to server fireup
        php artisan serve --port:your_server_port